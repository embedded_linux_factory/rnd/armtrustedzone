# ARM TrustedZone

## Description
The aim of this project is to develop a secure log application on the OP-TEE OS for display on the Rich OS (Linux) using Yocto.

## Objectives
- Produce a state of the art of OP-TEE and its components (In progress)
- Propose a simple mechanism for communication between the ARM TEE and the Linux kernel
- Develop a logging application on OPTEE-OS

## Authors
Nathanaël MARIE - @N.MARIE (Intern at the end of an CS Master)

Mohamed BELOUARGA - @belouargamohamed (Internship tutor)